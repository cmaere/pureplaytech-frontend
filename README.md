# Ionic App Task

For this project you will need to create an ionic 4 or above project with will have the following
- login screen with a login button and register button
- register page which will create a new user with an username and password
-  a dashboard which will display user details 
-  connect to a websocket to display updates in realtime
![alt text1][logo]

[logo]: wireframe.png "Wireframe for the APP"


#Detailed Tasks Description
## Task 1

- Create a login module with a register page, which will allow users to create their user account. 
- The login module is expected to consume a REST-API on this link: https://cmaere2.pythonanywhere.com/auth/users/
- For the user registration you are required to use this method:

- **method:** POST
- **Request:** 
```json
{
			"username":"[your username]",
			"email" : "[your email]",
			"password" : "[your password]",
			"re_password" : "[retype your password]"
	
} 
```
## Task 2

- within the login module create a login page for existing users to login into the App
- The login module is expected to consume a REST-API on this link: https://cmaere2.pythonanywhere.com/auth/jwt/create/
- For the user Login you are required to use this method:

- **method:** POST
- **Request:** 
```json
{
			"username":"[created username]",
			"password": "[created password]"
}
```
- **Output:**
```json
{
    "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYwMjMzNTczNCwianRpIjoiMWUwNjg5ZjEyOWU5NDI1YTg1MGM5YzcwYzM5NWRjNDgiLCJ1c2VyX2lkIjoxfQ.XFEzJbTDkPccEdUItEOafKZNaNFaOCNj2IFQ_3IcIJk",
    "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjAyMjQ5NjM0LCJqdGkiOiIzMzIwODczMzU3MTU0NGUzYWZhYjFlMDYxZTlmODY1MyIsInVzZXJfaWQiOjF9.WqlKPzTL5w1IFWlHnWV_9U0Uim-UTUo129wZOHVZ9m4"
}
```
## Task 3

- After a user successfully login, the app should redirect to a dashboard page where the username of the user shall be displayed using the endpoint link: https://cmaere2.pythonanywhere.com/auth/users/me/

- **method:** POST
- **Header:** 
```json
{
			
		"Authorization": "Bearer [JWT TOKEN generate above on 'access']"
	
}
```

- **Header practical example:**
```json

{
	"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjAyMjQ5NjM0LCJqdGkiOiIzMzIwODczMzU3MTU0NGUzYWZhYjFlMDYxZTlmODY1MyIsInVzZXJfaWQiOjF9.WqlKPzTL5w1IFWlHnWV_9U0Uim-UTUo129wZOHVZ9m4"

}
```

## Task 4
- This is an important task. On the Dashboard page display a statistic table or field retrieved from a socketio service (websocket) from this link: https://websocket.hismalawi.org:5001/socket.io
- To achieve this in ionic use the socket.io client library in ionic to be able to connect to the websocket service
- Use a Channel named "pureplay"
- make sure you have used a subscribe service to display the socket.io data changes in realtime without refreshing the page.
- The output data of the websocket channel is expect to look like this:
```json
{
	"statistics": [Random integer number]
}
```










